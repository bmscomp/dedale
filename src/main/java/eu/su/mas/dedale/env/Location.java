package eu.su.mas.dedale.env;

import java.io.Serializable;

/**
 * 
 * @author hc
 *
 */
public interface Location extends Serializable {
	
	
	public String toString();
	
	/**
	 * 
	 * @return The unique identifier of the current location
	 */
	public String getLocationId();
	
	/**
	 * 
	 * @param l
	 * @return True if this and l are equals
	 */
	public boolean equals(Location l);
	
}