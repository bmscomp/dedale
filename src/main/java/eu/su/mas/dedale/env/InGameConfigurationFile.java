package eu.su.mas.dedale.env;

/**
 * Used to define some constant related to the game mechanisms.
 * No need to give access to this file to the end user
 * 
 * @author hc
 *
 */
public final class InGameConfigurationFile {
	
	public static int MAX_BACKPACKVALUE_VALUE=70;
	public static int MIN_BACKPACK_VALUE=5;
	
	public static Double PERCENTAGE_TREASURE_LOSS=0.25;

	public static int MAX_GOLD_VALUE=100;
	public static int MAX_DIAMOND_VALUE=1000;
	
}
