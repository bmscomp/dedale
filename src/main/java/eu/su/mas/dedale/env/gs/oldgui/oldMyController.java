package eu.su.mas.dedale.env.gs.oldgui;


import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.graphstream.ui.fx_viewer.FxViewPanel;

import javafx.fxml.FXML;
import java.awt.Desktop;

public class oldMyController {

	// When user click on myButton
	// this method will be called.
	
	//Game;
	@FXML private MenuItem configure;
	@FXML private MenuItem exit;
	
	//Map
	@FXML private MenuItem loadtopology;
	@FXML private MenuItem createtopoloy;
	@FXML private MenuItem savetopology;
	
	//Help;
	@FXML private MenuItem forum;
	@FXML private MenuItem tchat;
	@FXML private MenuItem website;
	@FXML private MenuItem about;
	
	//right pannel
	@FXML private AnchorPane right;

	
	/**************
	 * Game menu
	 *************/
	@FXML
	private void dedaleConfigure(ActionEvent event) {
		
	}
	
	@FXML
	private void dedaleStart(ActionEvent event) {
		
	}	
	
	@FXML
	private void handleExitAction(ActionEvent event) {
		System.out.println("exiting");
		System.exit(0);
		//Platform.exit();
	}

	/**@FXML
	public void dedaleHelp(ActionEvent event){
		System.out.println("Dedale site");
		openUrl("https://dedale.gitlab.io/");
		System.out.println("Post");
	}
**/
	
	
	/**************
	 * Map menu
	 *************/
	
	@FXML
	private void dedaleLoadTopo(ActionEvent event) {
		System.out.println("Not yet implemented through the GUI but you can do it in the configuration file");
	}
	
	@FXML
	private void dedaleCreateTopo(ActionEvent event) {
		System.out.println("Create a topology");
		openUrl("https://dedale.gitlab.io/page/tutorial/configureenv/");
	}	
	
	@FXML
	private void dedaleSaveTopo(ActionEvent event) {
		System.out.println("Not yet implemented through the GUI");
	}

	
	
	/**************
	 * Help menu
	 *************/
	
	
	@FXML
	public void dedaleAbout(ActionEvent event){
		System.out.println("About Dedale");
		openUrl("https://dedale.gitlab.io/page/about/");
	}
	
	@FXML
	public void dedaleWebsite(ActionEvent event){
		System.out.println("Dedale website");
		openUrl("https://dedale.gitlab.io/");
	}
	
	@FXML
	public void dedaleTchat(ActionEvent event){
		System.out.println("Dedale tchat");
		openUrl("https://discord.gg/JZVz6sR");
	}
	
	@FXML
	public void dedaleForum(ActionEvent event){
		System.out.println("Dedale forum");
		
	}
	
	

	/**
	 * Open a browser tab with the uri given in parameter. Launch a new thread to be javafx compliant.
	 * @param uri
	 */
	private void openUrl(String uri) {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			new Thread(() -> {
				try {
					Desktop.getDesktop().browse(new URI(uri));
				} catch (IOException | URISyntaxException e) {
					e.printStackTrace();
				}
			}).start();
		}
	}

	/**
	 * Trying to add the graph ref to the scene
	 * @param truc
	 */
	@SuppressWarnings("restriction")
	public synchronized void setGraph(FxViewPanel truc) {
		System.out.println(truc.scaleShapeProperty());
		right.getChildren().add(truc);
		truc.setScaleShape(true);
		truc.prefWidthProperty().bind(right.widthProperty());
		truc.prefHeightProperty().bind(right.heightProperty());		
	}

}