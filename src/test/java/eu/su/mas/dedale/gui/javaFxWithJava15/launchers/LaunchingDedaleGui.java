package eu.su.mas.dedale.gui.javaFxWithJava15.launchers;

import org.junit.Assert;

import eu.su.mas.dedale.gui.MyController;
import eu.su.mas.dedale.gui.javaFxWithJava15.testclass.DedaleGui;
import eu.su.mas.dedale.gui.javaFxWithJava15.testclass.JavaFxtest;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class LaunchingDedaleGui {

	public static void main(String[] args) {
		new Thread(() -> {
			Application.launch(DedaleGui.class, null);
		}).start();
	}

//	public static void main(String[] args) {
//		DedaleGui.main(args);
//	}
	
}
