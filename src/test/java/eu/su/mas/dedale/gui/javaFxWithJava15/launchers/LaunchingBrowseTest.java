package eu.su.mas.dedale.gui.javaFxWithJava15.launchers;

import org.junit.Assert;

import eu.su.mas.dedale.gui.MyController;
import eu.su.mas.dedale.gui.javaFxWithJava15.testclass.BrowseTest;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;

public class LaunchingBrowseTest {

//	public static void main(String[] args) {
//		new Thread(() -> {
//			Application.launch(BrowseTest.class, null);
//		}).start();
//	}

	public static void main(String[] args) {
		BrowseTest.main(args);
	}
}
